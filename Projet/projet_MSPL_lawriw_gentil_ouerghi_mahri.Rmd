---
title: "étude sur les possibles cause du taux de suicide"
date: "march 26, 2024"
output:
  html_document:
    df_print: paged
  pdf_document: default
---


table des matières

Introduction

  1 : Description de la question
  
  2 : Contexte / description des Datasets
  
  3 : Comment les datasets ont'il été obtenus?
  
Methodologie :

  4 : Processus de nettoyage des données
  
  5 : workflow Scientifique 
  
  6 : choix de représentation des données
  
  7 : Analyse en programmation lettrée
  
8 : Conclusion

9 : References



Introduction :
1 : Description de la question :


La question est  "les indicateurs suivants ont ils un lien direct avec le taux de suicide d'un pays ?"

Dans ce rapport nous allons essayer de trouver des possibles causes ou facteurs aggravant de la prévalence du taux de suicide dans les morts d'un pays.
Les facteurs suivants seront étudié :
- taux d'homicide
- PIB par personne
- latitude

Attention nous rappelons que ce rapport de recherche que des corrélations, qui ne prouvent pas une causalité.
Si une corrélation est trouvée, dans recherches supplémentaires seront demandées pour trouver un lien concret et une explication.



2 : Contexte / description des Datasets :


les datasets suivants on été utilisé :

Taux de morts par suicide :
https://ourworldindata.org/grapher/share-deaths-suicide?tab=map&country=~USA

Le pourcentage annuel des morts par suicides pour chaque pays, allant de 1990 a 2019
Sourcé depuis l'Institute for Health Metrics and Evaluation, Global Burden of Disease (2019)

Taux d'homicides :
https://ourworldindata.org/grapher/homicide-rate

Le taux annuel des morts par homicide pour 100000 habitants pour chaque pays, allant de 1990 a 2019
Sourcé depuis l'Institute for Health Metrics and Evaluation, Global Burden of Disease (2019)

PIB par personne :
https://ourworldindata.org/grapher/gdp-per-capita-worldbank?time=2021

Le PIB par habitant par années pour chaque pays, allant de 1990 to 2021, cependant les données passé 2019 seront ignorées car les autres datasets s'arrètes en 2019
Sourcé depuis la World Bank (2023)
Notes pour les valeurs monétaires utilisées par les données :
Exprimé en dollar international de 2017
Ajusté pour inflation et coups de vie dans les pays


Latitude et longitude des pays : 
https://gist.github.com/tadast/8827699

liste des pays avec latitude et longitude, cependant ce rapport ne ce port que sur la latitude
Sourcée depuis le github de Tadas Tamošauskas 
ATTENTION : ces données proviennent d'un projet d'un developper et non d'un organisme mondialement reconnu, prudence et vérifications des données requises



```{r}
library(tidyverse)
library(dplyr);
library(magrittr);
library(ggplot2);

kms <- read.csv("share-deaths-suicide.csv")
names(kms) <- c("Country", "Code", "Year", "Suicide Rate")
hr <- read.csv("homicide-rate.csv")
names(hr) <- c("Country", "Code", "Year", "Homicide Rate")
pib <- read.csv("gdp-per-capita-worldbank.csv")
names(pib) <- c("Country", "Code", "Year", "PIB per capita")
position <- read.csv("countries_codes_and_coordinates.csv")
names(position) <- c("Country", "Code2", "Code3", "Number", "Lat", "Long")

deathsMerge <- merge(kms, hr, by=c("Country", "Year"), all = TRUE)
deathsPIBMerge <- merge(deathsMerge, pib, by=c("Country", "Year"), all = TRUE)
fullmerge <- merge(deathsPIBMerge, position, by=c("Country"), all = TRUE)

```

3 : Comment les datasets ont'il été obtenus? :


Tous les datasets sauf celui concernant les latitudes on été récupéré sur le site "our world in data".
Le set concernant les latitudes a été récupéré sur github.


Methodologie :


4 : Processus de nettoyage des données :

Pour commencer, les données en dehors de 1990-2019 seront ignorées car le dataset des taux de suicides n'existe passé ce point.
Les longitudes ne seront également pas prises en compte car l'on ne recherche que l'influence de la latitude.
 
Tous les datasets ont été combiné par pays par années

Attention, il y a des rares trous dans le datasets du PIB par habitant, les pays concernés ne seront étudié sous cet angle.


5 : workflow Scientifique :


après avoir définis la question des possibles causes ou facteurs agravants du taux de suicide, nous avons obtenus les datasets nécéssaires pour l'analyse. 

enfins, la réalisation des graphes :
nous avons choisi de réaliser un graphe global  pour chaque facteur possible ainsi qu'une analyse de cas pour des pays que nous consideront comme intérèssant statistiquement. 



6 : choix de représentation des données :

nous avons choisi en premiers lieu une analyse mondiale avec les nuages de points suivants.

- taux de suicide en fonction de latitude
- PIB par habitant en fonction de latitude
- PIB par habitant en fonction de latitude
- taux de suicide en fonction du taux de meutres 
- taux de suicide en fonction du PIB par habitant

nous avons aussi décider de réaliser un étude de cas sur des pays qui nous paraitrait pertinant.
pour chaque pays nous avons créé en graphe représentant l'évolution au cour de temp :

- taux de suicide en fonction du temp
- taux de meutres en fonction du temp
- taux de PIB en fonction du temp


les pays sélectionné pour étude de cas sont :
- el-salvador (représenté en noir), car cela permet d'étudier l'effet d'un taux de meutre anormalement élevé sur le taux de suicide
- la france (représentée en bleu), pour avoir le cas domestique
- les etats unis d'amérique (représentés en vert) ainsi que la chine (représentée en rouge), qui sont les principaux pôle économique mondiaux


nous avons aussi décidé de faire des études de cas suplémentaires groupé par NATO et BRIC :
certain pays peuvent apparaitres dans les deux études de cas, notez qu'il peuvent avoir une couleur différente.

NATO :
- USA : darkgoldenrod
- Canada : rouge
- France : bleu
- UK : chartreuse
- Germany : orange

BRICS

- Brazil : vert
- Russia : bleu
- India : orange
- China : rouge
- South Africa : cyan


```{r}
salva <- fullmerge %>% select("Country", "Year", "Suicide Rate", "Homicide Rate", "PIB per capita", "Lat") %>% filter(Country == "El Salvador")
france <- fullmerge %>% select("Country", "Year", "Suicide Rate", "Homicide Rate", "PIB per capita", "Lat") %>% filter(Country == "France")
chine <- fullmerge %>% select("Country", "Year", "Suicide Rate", "Homicide Rate", "PIB per capita", "Lat") %>% filter(Country == "China")
usa <- fullmerge %>% select("Country", "Year", "Suicide Rate", "Homicide Rate", "PIB per capita", "Lat") %>% filter(Country == "United States")
sa <- fullmerge %>% select("Country", "Year", "Suicide Rate", "Homicide Rate", "PIB per capita", "Lat") %>% filter(Country == "South Africa")
gro <- fullmerge %>% select("Country", "Year", "Suicide Rate", "Homicide Rate", "PIB per capita", "Lat") %>% filter(Country == "Greenland")
bra <- fullmerge %>% select("Country", "Year", "Suicide Rate", "Homicide Rate", "PIB per capita", "Lat") %>% filter(Country == "Brazil")
rus <- fullmerge %>% select("Country", "Year", "Suicide Rate", "Homicide Rate", "PIB per capita", "Lat") %>% filter(Country == "Russia")
ind <- fullmerge %>% select("Country", "Year", "Suicide Rate", "Homicide Rate", "PIB per capita", "Lat") %>% filter(Country == "India")
uk <- fullmerge %>% select("Country", "Year", "Suicide Rate", "Homicide Rate", "PIB per capita", "Lat") %>% filter(Country == "United Kingdom")
ger <- fullmerge %>% select("Country", "Year", "Suicide Rate", "Homicide Rate", "PIB per capita", "Lat") %>% filter(Country == "Germany")
can <- fullmerge %>% select("Country", "Year", "Suicide Rate", "Homicide Rate", "PIB per capita", "Lat") %>% filter(Country == "Canada")



elsalva <- data.frame(
  year = c(salva %>% select("Year")),
  suicideRate = c(salva %>% select("Suicide Rate")),
  homicideRate = c(salva %>% select("Homicide Rate")),
  pibcapita = c(salva %>% select("PIB per capita"))
)
elsalva <- elsalva[!(elsalva$Year>"2019"),]

lafrance <- data.frame(
  year = c(france %>% select("Year")),
  suicideRate = c(france %>% select("Suicide Rate")),
  homicideRate = c(france %>% select("Homicide Rate")),
  pibcapita = c(france %>% select("PIB per capita"))
)
lafrance <- lafrance[!(lafrance$Year>"2019"),]

lachine <- data.frame(
  year = c(chine %>% select("Year")),
  suicideRate = c(chine %>% select("Suicide Rate")),
  homicideRate = c(chine %>% select("Homicide Rate")),
  pibcapita = c(chine %>% select("PIB per capita"))
)
lachine <- lachine[!(lachine$Year>"2019"),]

lesusa <- data.frame(
  year = c(usa %>% select("Year")),
  suicideRate = c(usa %>% select("Suicide Rate")),
  homicideRate = c(usa %>% select("Homicide Rate")),
  pibcapita = c(usa %>% select("PIB per capita"))
)
lesusa <- lesusa[!(lesusa$Year>"2019"),]

southafrica <- data.frame(
  year = c(sa %>% select("Year")),
  suicideRate = c(sa %>% select("Suicide Rate")),
  homicideRate = c(sa %>% select("Homicide Rate")),
  pibcapita = c(sa %>% select("PIB per capita"))
)
southafrica <- southafrica[!(southafrica$Year>"2019"),]

greenland <- data.frame(
  year = c(gro %>% select("Year")),
  suicideRate = c(gro %>% select("Suicide Rate")),
  homicideRate = c(gro %>% select("Homicide Rate")),
  pibcapita = c(gro %>% select("PIB per capita"))
)
greenland <- greenland[!(greenland$Year>"2019"),]

lebresil <- data.frame(
  year = c(bra %>% select("Year")),
  suicideRate = c(bra %>% select("Suicide Rate")),
  homicideRate = c(bra %>% select("Homicide Rate")),
  pibcapita = c(bra %>% select("PIB per capita"))
)
lebresil <- lebresil[!(lebresil$Year>"2019"),]

larussie <- data.frame(
  year = c(rus %>% select("Year")),
  suicideRate = c(rus %>% select("Suicide Rate")),
  homicideRate = c(rus %>% select("Homicide Rate")),
  pibcapita = c(rus %>% select("PIB per capita"))
)
larussie <- larussie[!(larussie$Year>"2019"),]

linde <- data.frame(
  year = c(ind %>% select("Year")),
  suicideRate = c(ind %>% select("Suicide Rate")),
  homicideRate = c(ind %>% select("Homicide Rate")),
  pibcapita = c(ind %>% select("PIB per capita"))
)
linde <- linde[!(linde$Year>"2019"),]

leroyaumeuni <- data.frame(
  year = c(uk %>% select("Year")),
  suicideRate = c(uk %>% select("Suicide Rate")),
  homicideRate = c(uk %>% select("Homicide Rate")),
  pibcapita = c(uk %>% select("PIB per capita"))
)
leroyaumeuni <- leroyaumeuni[!(leroyaumeuni$Year>"2019"),]

lallemagne <- data.frame(
  year = c(ger %>% select("Year")),
  suicideRate = c(ger %>% select("Suicide Rate")),
  homicideRate = c(ger %>% select("Homicide Rate")),
  pibcapita = c(ger %>% select("PIB per capita"))
)
lallemagne <- lallemagne[!(lallemagne$Year>"2019"),]

lecanada <- data.frame(
  year = c(can %>% select("Year")),
  suicideRate = c(can %>% select("Suicide Rate")),
  homicideRate = c(can %>% select("Homicide Rate")),
  pibcapita = c(can %>% select("PIB per capita"))
)
lecanada <- lecanada[!(lecanada$Year>"2019"),]

```

7 : Analyse en programmation lettrée :




voici si dessous l'analyse globale,
dans un premier temp, voici les différents facteurs étudié répartis par latitude pour chaque pays, pour l'année la plus récente du jeux de donnés (2019) 
```{r}


# Suicide rate for the year 2019
fullmerge_2019 <- filter(fullmerge, Year == 2019)

ggplot(fullmerge_2019, aes(x = Lat, y = `Suicide Rate`)) +
  geom_point( alpha = 0.5) +
  labs(title = "Suicide Rate by Latitude (2019)",
       x = "Latitude",
       y = "Suicide Rate")



# Homicide rate for the year 2019
ggplot(fullmerge_2019, aes(x = Lat, y = `Homicide Rate`)) +
  geom_point( alpha = 0.5) +
  labs(title = "Homicide Rate by Latitude (2019)",
       x = "Latitude",
       y = "Homicide Rate")



# PIB per capita for the year 2019
ggplot(fullmerge_2019, aes(x = Lat, y = `PIB per capita`)) +
  geom_point( alpha = 0.5) +
  labs(title = "PIB per capita by Latitude (2019)",
       x = "Latitude",
       y = "PIB per capita")


```

voici le taux de suicide répartis par le taux d'homicides pour chaque pays, pour l'année la plus récente du jeux de donnés (2019) 

```{r}

# suicide by Homicide rate for the year 2019
ggplot(fullmerge_2019, aes(x = `Suicide Rate` , y = `Homicide Rate`)) +
  geom_point( alpha = 0.5) +
  labs(title = "suicide rate by Homicide Rate (2019)",
       x = "Suicide Rate",
       y = "Homicide Rate")

```

voici le taux de suicide répartis par lePIB par habitans pour chaque pays, pour l'année la plus récente du jeux de donnés (2019) 


```{r}
# suicide by PIB per capita for the year 2019
ggplot(fullmerge_2019, aes(x = `Suicide Rate` , y = `PIB per capita`)) +
  geom_point( alpha = 0.5) +
  labs(title = "suicide rate by PIB per capita (2019)",
       x = "Suicide Rate",
       y = "PIB per capita")
```


```{r}

ggplot() +
  geom_line(data = elsalva, aes(x=Year, y=Suicide.Rate), color = "black") +
  geom_line(data = lafrance, aes(x=Year, y=Suicide.Rate), color = "blue") +
  geom_line(data = lachine, aes(x=Year, y=Suicide.Rate), color = "red") +
  geom_line(data = lesusa, aes(x=Year, y=Suicide.Rate), color = "green") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_colour_manual("", 
                      breaks = c("El Salvador", "France", "China", "USA"),
                      values = c("black", "blue", "red", "green")) +
  xlab("Years") +
  ylab("Suicide rate") +
  labs(title="Suicide rate per Year")  

```

veuillez noter que le taux de suicides du groenland est tel qu'il est impossible de le représenter sur le même graphe que les autres pays de ces études de cas.


voici le graphe du taux de suicide du groenland:


```{r}



ggplot() +
  geom_line(data = greenland, aes(x=Year, y=Suicide.Rate), color = "deeppink") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_colour_manual("", 
                      breaks = c("Greenland"),
                      values = c("deeppink")) +
  xlab("Years") +
  ylab("Suicide rate") +
  labs(title="Suicide rate per Year in Greenland")


```


voici les graphes des PIB par habitants des pays séléctionné :
NOTE : groenland n'a pas de données a se sujet.

```{r}


ggplot() +
  geom_line(data = elsalva, aes(x=Year, y=PIB.per.capita), color = "black") +
  geom_line(data = lafrance, aes(x=Year, y=PIB.per.capita), color = "blue") +
  geom_line(data = lachine, aes(x=Year, y=PIB.per.capita), color = "red") +
  geom_line(data = lesusa, aes(x=Year, y=PIB.per.capita), color = "green") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_colour_manual("", 
                      breaks = c("El Salvador", "France", "China", "USA"),
                      values = c("black", "blue", "red", "green")) +
  xlab("Years") +
  ylab("PIB per capita") +
  labs(title="PIB per capita per Year")  
```


voici le taux d'homicides des pays séléctionné :

```{r}

ggplot() +
  geom_line(data = lafrance, aes(x=Year, y=Homicide.Rate), color = "blue") +
  geom_line(data = lachine, aes(x=Year, y=Homicide.Rate), color = "red") +
  geom_line(data = lesusa, aes(x=Year, y=Suicide.Rate), color = "green") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_colour_manual("", 
                      breaks = c("France", "China", "USA"),
                      values = c("blue", "red", "green")) +
  xlab("Years") +
  ylab("Homicide rate") +
  labs(title="Homicide rate per Year") 




```

veuillez noter que le taux d'homicide de el salvador et du groenland sont tel qu'il est impossible de les représenter sur le même graphe que les autres pays de ces études de cas.



voici le graphe du taux d'homicide de el-salvador:

```{r}


ggplot() +
  geom_line(data = elsalva, aes(x=Year, y=Homicide.Rate), color = "black") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_colour_manual("", 
                      breaks = c("El Salvador"),
                      values = c("black")) +
  xlab("Years") +
  ylab("Homicide rate") +
  labs(title="Homicide rate per Year in El Salvador") 

```

voici le graphe du taux d'homicide du groenland:

```{r}

ggplot() +
  geom_line(data = greenland, aes(x=Year, y=Homicide.Rate), color = "deeppink") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_colour_manual("", 
                      breaks = c("Greenland"),
                      values = c("deeppink")) +
  xlab("Years") +
  ylab("Homicide rate") +
  labs(title="Homicide rate per Year in Greenland")


```

voici la seconde étude de cas : une comparaison en NATO et BRICS

taux de suicide :
```{r}

ggplot() +
  geom_line(data = lebresil, aes(x=Year, y=Suicide.Rate), color = "green") +
  geom_line(data = larussie, aes(x=Year, y=Suicide.Rate), color = "blue") +
  geom_line(data = linde, aes(x=Year, y=Suicide.Rate), color = "orange") +
  geom_line(data = lachine, aes(x=Year, y=Suicide.Rate), color = "red") +
  geom_line(data = southafrica, aes(x=Year, y=Suicide.Rate), color = "cyan") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_colour_manual("", 
                      breaks = c("Brazil", "Russia", "India", "China", "South Africa"),
                      values = c("green", "blue", "orange", "red", "cyan")) +
  xlab("Years") +
  ylab("Suicide rate") +
  labs(title="Suicide rate per Year in the BRICS") 

ggplot() +
  geom_line(data = lesusa, aes(x=Year, y=Suicide.Rate), color = "darkgoldenrod") +
  geom_line(data = lecanada, aes(x=Year, y=Suicide.Rate), color = "red") +
  geom_line(data = lafrance, aes(x=Year, y=Suicide.Rate), color = "blue") +
  geom_line(data = leroyaumeuni, aes(x=Year, y=Suicide.Rate), color = "chartreuse") +
  geom_line(data = lallemagne, aes(x=Year, y=Suicide.Rate), color = "orange") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_colour_manual("", 
                      breaks = c("USA", "Canada", "France", "United Kingdom", "Germany"),
                      values = c("darkgoldenrod", "red", "blue", "chartreuse", "orange")) +
  xlab("Years") +
  ylab("Suicide rate") +
  labs(title="Suicide rate per Year in NATO") 


```

comparaison pour le taux d'homicide
 
note : pour des raison de claretée visuelle, les USA sont représentés sur leur propres graphe avec leur propre échelle

```{r}


ggplot() +
  geom_line(data = lebresil, aes(x=Year, y=Homicide.Rate), color = "green") +
  geom_line(data = larussie, aes(x=Year, y=Homicide.Rate), color = "blue") +
  geom_line(data = linde, aes(x=Year, y=Homicide.Rate), color = "orange") +
  geom_line(data = lachine, aes(x=Year, y=Homicide.Rate), color = "red") +
  geom_line(data = southafrica, aes(x=Year, y=Homicide.Rate), color = "cyan") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_colour_manual("", 
                      breaks = c( "Brazil", "Russia", "India", "China", "South Africa"),
                      values = c("green", "blue", "orange", "red", "cyan")) +
  xlab("Years") +
  ylab("Homicide rate") +
  labs(title="Homicide rate per Year in the BRICS") 





ggplot() +
  geom_line(data = lecanada, aes(x=Year, y=Homicide.Rate), color = "red") +
  geom_line(data = lafrance, aes(x=Year, y=Homicide.Rate), color = "blue") +
  geom_line(data = leroyaumeuni, aes(x=Year, y=Homicide.Rate), color = "chartreuse") +
  geom_line(data = lallemagne, aes(x=Year, y=Homicide.Rate), color = "orange") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_y_continuous(breaks = seq(0, 3, by=0.2)) +
  scale_colour_manual("", 
                      breaks = c("Canada", "France", "United Kingdom", "Germany"),
                      values = c("red", "blue", "chartreuse", "orange")) +
  xlab("Years") +
  ylab("Homicide rate") +
  labs(title="Homicide rate per Year in NATO, US excluded")



ggplot() +
  geom_line(data = lesusa, aes(x=Year, y=Homicide.Rate), color = "darkgoldenrod") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_colour_manual("", 
                      breaks = c("USA"),
                      values = c("darkgoldenrod")) +
  xlab("Years") +
  ylab("Homicide rate") +
  labs(title="Homicide rate per Year in the USA")

```

voici la compéraison par PIB par habitants :

```{r}


ggplot() +
  geom_line(data = lebresil, aes(x=Year, y=PIB.per.capita), color = "green") +
  geom_line(data = larussie, aes(x=Year, y=PIB.per.capita), color = "blue") +
  geom_line(data = linde, aes(x=Year, y=PIB.per.capita), color = "orange") +
  geom_line(data = lachine, aes(x=Year, y=PIB.per.capita), color = "red") +
  geom_line(data = southafrica, aes(x=Year, y=PIB.per.capita), color = "cyan") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_y_continuous(breaks = seq(0, 40000, by=5000)) +
  scale_colour_manual("", 
                      breaks = c("Brazil", "Russia", "India", "China", "South Africa"),
                      values = c("green", "blue", "orange", "red", "cyan")) +
  xlab("Years") +
  ylab("GDP per capita") +
  labs(title="GDP per capita per Year in the BRICS") 

ggplot() +
  geom_line(data = lesusa, aes(x=Year, y=PIB.per.capita), color = "darkgoldenrod") +
  geom_line(data = lecanada, aes(x=Year, y=PIB.per.capita), color = "red") +
  geom_line(data = lafrance, aes(x=Year, y=PIB.per.capita), color = "blue") +
  geom_line(data = leroyaumeuni, aes(x=Year, y=PIB.per.capita), color = "chartreuse") +
  geom_line(data = lallemagne, aes(x=Year, y=PIB.per.capita), color = "orange") +
  scale_x_continuous(breaks = seq(1990, 2019, by=2)) +
  scale_y_continuous(breaks = seq(0, 70000, by=5000)) +
  scale_colour_manual("", 
                      breaks = c("USA", "Canada", "France", "United Kingdom", "Germany"),
                      values = c("darkgoldenrod", "red", "blue", "chartreuse", "orange")) +
  xlab("Years") +
  ylab("GDP per capita") +
  labs(title="GDP per capita per Year in NATO") 

```




8 : Conclusion


Voici les résultats trouvés quant aux trois possibles facteurs aggravants de taux de suicide d'un pays :

De par notre analyse globale, l'on peut conclure qu'il ne semble pas y avoir de lien tangible entre la latitude d'un pays et le taux de suicide.
La répartition des taux de suicides de forme pas de variation remarquable en fonction de la latitude.
Excepté le cas du Groenland qui a un taux de suicide anormalement élevé.


À premier coup d'œil il semblerait qu'au cours des dernières années une augmentation du PIB a coïncidée avec une baisse du taux de suicide sur les pays observé.
cependant, sur les pays observés, des variations dans le taux de suicides de se retrouvent pas dans l'évolution du PIB.
De plus, sur l'année 2019 les deux pays observés dans la première étude de cas avec le taux de suicide le plus élevé, la France et les USA, ont également le PIB le plus élevé.

pour le taux d'homicide, les données ne prouvent pas une corrélation directe.
Dans la première analyses de cas, une baisse des taux d'homicides semble être corrélée avec une baisse des taux de suicides. 
Nous avons également étudié le cas des USA, ou le taux d'homicides et le taux de suicides ont suivis une augmentation passé l'an 2000 suivit d'une baisse passé l'an 2018 
Cependant, comme prouvé par l'analyse globale, les pays avec le taux de homicide le plus élevé ne sont pas forcément ceux avec le taux de suicide le plus élevé.
Nous avons aussi observé grâce a l'analyse du cas d'el-slavador qu'un pic du taux de homicide peut baisser le pourcentage de suicides simplement car les homicides prennent une plus grosse part des morts.



En conclusion, parmi les trois facteurs étudié, il n'a pas été trouvé de lien statistique suffisante pour conclure une influence directe d'un de ces facteurs sur le taux de suicide d'un pays.
Dans le futur, d'autres facteurs à étudier serait par exemple les autres composants de l'indice de développement d'un pays, comme l'espérance de vie ou le niveaux d'éducation.



9 : References

site our world in data :
https://ourworldindata.org

World bank's databank :
https://datacatalog.worldbank.org/search/dataset/0037712/World-Development-Indicators

site of the Global Burden of Disease, by the Institute for Health Metrics and Evaluation :
https://www.healthdata.org/research-analysis/gbd

datasets pays coordonées :
https://gist.github.com/tadast/8827699
